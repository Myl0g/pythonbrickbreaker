# Milo Gilad
# 11/16/16
# Atari Breakout
from graphics import *
from tkinter import *
from random import randint
import graphics
import random
def main():
    win = GraphWin("The One and Only Neon Brick Breaker™ © Milo Gilad 2002-2102", 800, 600) # Setting Up (width, height)
    win.setBackground("Black")
    bricks = []
    colors = ['Green', 'Yellow', 'Red', 'Blue', 'Orange', 'Purple', 'Cyan', 'Green', 'Brown', 'Gray']
    x = 0
    y = 45
    for j in range(6):
        for i in range(10):
            x2 = x+80
            y2 = y+30
            rect = Rectangle(Point(x, y), Point(x2, y2))
            rect.setOutline(colors[i])
            rect.draw(win)
            bricks.append(rect)
            x+=80
        x = 0
        y+=30
    paddle = Rectangle(Point(350, 500), Point(445, 520)) # Making Paddle
    paddle.setFill("White")
    paddle.draw(win)
    ball = Circle(Point(385, 485), 15) # Making Ball
    ball.setFill("White")
    ball.draw(win)
    ballX = 7
    ballY = -(randint(10, 15))
    lifeCount = 9999 # Making Life Counter
    lifeDisplay = graphics.Text(Point(400, 30), "Lives: " + str(lifeCount))
    lifeDisplay.setFill("White")
    lifeDisplay.setSize(36)
    lifeDisplay.draw(win)
    while True:
        location = win.winfo_pointerx() # Moving Paddle
        q = location - paddle.getCenter().getX()
        paddle.move(q, 0)
        ball.move(ballX, ballY) # Making Ball Bounce Around
        if ball.getCenter().getY() > 600:
            lifeDisplay.undraw()
            ball.undraw()
            ball.draw(win)
            ballY = -ballY
            lifeCount = lifeCount - 1
            lifeDisplay = graphics.Text(Point(400, 30), "Lives: " + str(lifeCount))
            lifeDisplay.setFill("White")
            lifeDisplay.setSize(36)
            lifeDisplay.draw(win)
        if ball.getCenter().getY() < 0:
            ballY = -ballY
        if ball.getCenter().getX() > 800:
            ballX = -ballX
        if ball.getCenter().getX() < 0:
            ballX = -ballX
        if ball.getCenter().getX() - 15 >= paddle.getP1().getX() and ball.getCenter().getX() - 15 <= paddle.getP2().getX() and ball.getCenter().getY() == 485:
            ballY = -ballY
        if lifeCount <= 0:
            print("Game Over")
            quit()
        if ball.getCenter().getY() <= 225:
            for i in bricks:
                if i.getP1().getX() <= ball.getCenter().getX() and i.getP2().getX() >= ball.getCenter().getX():
                    if i.getP2().getY() >= ball.getCenter().getY() and i.getP1().getY() >= ball.getCenter().getY():
                        i.undraw()
                        ballY = -ballY
                        bricks.remove(i)
                        if len(bricks) == 0:
                            ball.undraw()
                            youWin = graphics.Text(Point(400, 300), "You Win!")
                            youWin.setSize(33)
                            youWin.setFill("White")
                            youWin.draw(win)
                            exit()
##            for i in range(60):
##                if bricks[i]==False:
##                    break
##                else:
##                    if ball.getCenter().getX() <= bricks[i].getP2().getX() and ball.getCenter().getX() >= bricks[i].getP1().getX() and ball.getCenter().getY() >= bricks[i].getP2().getY() and ball.getCenter().getY() <= bricks[i].getP1().getY():
##                        bricks[i].undraw()
##                        del bricks[i]
##                        bricks.insert(i, False)
##                        ballY = -ballY
main()
