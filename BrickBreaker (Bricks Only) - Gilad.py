# Milo Gilad
# 11/16/16
# Atari Breakout

from graphics import *
from tkinter import *

def makeBricks(win):
    colors = ['Green', 'Yellow', 'Red', 'Blue', 'Orange', 'Purple', 'White', 'Green', 'Brown', 'Gray']
    x = 0
    y = 100
    for i in range(6):
        for i in range(10):
            rect = Rectangle(Point(x, y), Point(x+75, y-30))
            rect.setOutline(colors[i])
            rect.draw(win)
            x+=80
        x = 0
        y+=30
def main():
    win = GraphWin("Neon Atari Brick Breaker o' Wonders", 800, 600)
    win.setBackground("Black")
    makeBricks(win)
main()
