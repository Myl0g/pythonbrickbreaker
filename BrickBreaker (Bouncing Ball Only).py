# Milo Gilad
# 11/16/16
# Atari Breakout
from graphics import *
from tkinter import *
def main():
    win = GraphWin("The One and Only Neon Brick Breaker™", 800, 600) # Setting Up
    win.setBackground("Black")
    bricks = []
    colors = ['Green', 'Yellow', 'Red', 'Blue', 'Orange', 'Purple', 'Cyan', 'Green', 'Brown', 'Gray']
    x = 0
    y = 75
    for j in range(6):
        for i in range(10):
            x2 = x+75
            y2 = y-30
            rect = Rectangle(Point(x, y), Point(x2, y2))
            rect.setOutline(colors[i])
            rect.draw(win)
            bricks.append(rect)
            x+=80
        x = 0
        y+=30
    paddle = Rectangle(Point(350, 500), Point(425, 520)) # Making Paddle
    paddle.setFill("White")
    paddle.draw(win)
    ball = Circle(Point(385, 485), 15) # Making Ball
    ball.setFill("White")
    ball.draw(win)
    ballX = 7
    ballY = 8
    while True:
        location = win.winfo_pointerx() # Moving Paddle
        q = location - paddle.getCenter().getX()
        paddle.move(q, 0)
        ball.move(ballX, ballY) # Making Ball Bounce Around
        if ball.getCenter().getY() > 600:
            ballY = -ballY
        if ball.getCenter().getY() < 0:
            ballY = -ballY
        if ball.getCenter().getX() > 800:
            ballX = -ballX
        if ball.getCenter().getX() < 0:
            ballX = -ballX
        if ball.getCenter().getX() >= paddle.getP1().getX() and ball.getCenter().getX() <= paddle.getP2().getX() and ball.getCenter().getY() == 485:
            ballY = -ballY
main()
